import java.util.*;
%%
%class simpleC2
%standalone
Numbers = ({Num1}|{Num2}|{Num3}) {Exp}?

Num1=[0-9]+\.[0-9]*
Num2=\.[0-9]+
Num3=[0-9]+
Exp=[eE][+-]?[0-9]+

Words = [a-zA-Z]+ 
Comment = "/*" [^*] ~"*/"
WhiteSpace = [ \t\n]

/* keywords */
CHAR = "char"
INT = "int"
FLOAT = "float"
IF = "if"
ELSE = "else"
WHILE = "while"
PRINT = "print"
READ = "read"
RETURN = "return"
FUNC = "func"
PROGRAM = "program"
END = "end"

/* symbols */
SEMICOLON = ";"
LPAREN = "("
RPAREN = ")"
LBRACK = "["
RBRACK = "}"
LBRACE = "{"
RBRACE = "}"
EQ = "="
PLUS = "+"
MINUS = "-"
MULT = "*"
DIV = "/"
LT = "<"
GT = ">"
LTEQ = "<="
GTEQ = ">="
NOTEQ = "!="
ANDAND = "&&"
OROR = "||"
NOT = "!"

%%
    
  /* comments */
  {Comment}                      { /* ignore */ }

  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }

{Numbers} {System.out.println("Found number: " + yytext());}
/* Keywords */
{CHAR} {System.out.println("Found keyword: " + yytext());}
{INT} {System.out.println("Found keyword: " + yytext());}
{FLOAT} {System.out.println("Found keyword: " + yytext());}
{IF} {System.out.println("Found keyword: " + yytext());}
{ELSE} {System.out.println("Found keyword: " + yytext());}
{WHILE} {System.out.println("Found keyword: " + yytext());}
{PRINT} {System.out.println("Found keyword: " + yytext());}
{READ} {System.out.println("Found keyword: " + yytext());}
{RETURN} {System.out.println("Found keyword: " + yytext());}
{FUNC} {System.out.println("Found keyword: " + yytext());}
{PROGRAM} {System.out.println("Found keyword: " + yytext());}
{END} {System.out.println("Found keyword: " + yytext());}

/* Symbols */
{SEMICOLON} {System.out.println("Found symbol: " + yytext());}
{LPAREN} {System.out.println("Found symbol: " + yytext());}
{RPAREN} {System.out.println("Found symbol: " + yytext());}
{LBRACK} {System.out.println("Found symbol: " + yytext());}
{RBRACK} {System.out.println("Found symbol: " + yytext());}
{LBRACE} {System.out.println("Found symbol: " + yytext());}
{RBRACE} {System.out.println("Found symbol: " + yytext());}
{EQ} {System.out.println("Found symbol: " + yytext());}
{PLUS} {System.out.println("Found symbol: " + yytext());}
{MINUS} {System.out.println("Found symbol: " + yytext());}
{MULT} {System.out.println("Found symbol: " + yytext());}
{DIV} {System.out.println("Found symbol: " + yytext());}
{LT} {System.out.println("Found symbol: " + yytext());}
{GT} {System.out.println("Found symbol: " + yytext());}
{LTEQ} {System.out.println("Found symbol: " + yytext());}
{GTEQ} {System.out.println("Found symbol: " + yytext());}
{NOTEQ} {System.out.println("Found symbol: " + yytext());}
{ANDAND} {System.out.println("Found symbol: " + yytext());}
{OROR} {System.out.println("Found symbol: " + yytext());}
{NOT} {System.out.println("Found symbol: " + yytext());}

{Words} {System.out.println("Found word: " + yytext());}

\n { /* Do Nothing */}
. {/* Do Nothing */}