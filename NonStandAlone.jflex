/**
 * This is a simple example of a jflex lexer definition
 * that is not a standalone application
 */

/* Declarations */


%%

%class  MyScannerTwo   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   Token      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
/* Patterns */

other         = .
letter        = [A-Za-z]
word          = {letter}+
whitespace    = [ \n\t]

/**
%%

[0-9]+					printf("Number ");
[0-9]+"."[0-9]			printf("Decimal_Number ");
"."						printf("Dot "):

%%
**/

%%
/* Lexical Rules */

{word}     {
             /** Print out the word that was found. */
             //System.out.println("Found a word: " + yytext());
             return( new Token( yytext()));
            }
            
{whitespace}  {  /* Ignore Whitespace */ 
                 
              }

{other}    { 
             System.out.println("Illegal char: '" + yytext() + "' found.");
           }
           