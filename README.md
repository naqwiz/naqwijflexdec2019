## Scanner Development Using JFlex

The goal of this project is to create a scanner which returns tokens for identifiers, numbers, and all the keywords and symbols pertaining to a simple C-language. The scanner returns comments as a COMMENT token.

The scanner is created using the JFlex tool.

**Getting Started**

Please obtain a copy of the project to run it from command prompt. The scanner generator is named SimpleC.flex. 

**Prerequisites**

You need JFlex 1.7.0, which can be downloaded from https://www.jflex.de/download.html. You can use any Java Development Kit for testing and further development. JDK1.8.0_221 together with JRE1.8.0_221 were used for the initial development.

**Running the tests**

Please run tests to check if the application correctly returns tokens for identifiers, numbers, keywords, symbols and comments. Report any errors to the author below. Tests can be run using the following steps:

1. Prepare a text file with some code.

2. Generate the scanner using jflex SimpleC.flex. This should create simpleC.java, which is a Java class.

3. Compile the Java code using javac simpleC.java.

4. Scan your text file using the command java simpleC <text file>.

5. The results should show up in the command prompt.

**Built With**

This application is built with JFlex1.7.0 and JDK1.8.0_221.


**Author**

Zoha Naqwi


**Acknowledgments**

Author acknowledges the guidance and support provided by Prof. Erik Steinmetz for development of this application.
